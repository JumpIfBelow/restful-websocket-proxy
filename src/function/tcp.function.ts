import * as net from 'net';
import { Observable, Observer, Subject } from 'rxjs';

/**
 * Creates a TCP socket with the given URI.
 * @param host {string} A string representing the host to connect to
 * @param port {number} An integer representing the port to connect to
 * @returns {Subject<Buffer>} A new TCP socket client.
 */
export function tcp(host: string, port: number): Subject<Buffer> {
    const client = new net.Socket();
    client.connect(port, host);

    const observable = new Observable<Buffer>(
        subscriber => {
            client.on('data', data => subscriber.next(data));

            return () => {
                client.destroy();
            };
        },
    );

    const observer: Observer<Buffer> = {
        next: data => client.write(data),
        error: error => console.error(error),
        complete: () => console.info('TCP emission complete.'),
    };

    return Subject.create(observer, observable);
}
