import { EqualsInterface } from "@interface/equals.interface";
import { Socket } from "./socket.class";
import { WebSocket } from "ws";

export class SocketProxy implements EqualsInterface, ToJsonInterface {
    public readonly webSocketClients: WebSocket[] = [];

    public constructor(
        public readonly host: string,
        public readonly port: number,
        public readonly client: string,
        public readonly socketServer: Socket,
    ) {
    }

    public getUri(): string {
        return `ws://${this.host}:${this.port}`;
    }

    public equals(v: any): boolean {
        return this === v
            || v instanceof SocketProxy
            && this.host === v.host
            && this.port === v.port
            && this.client === v.client
            && this.socketServer.equals(v.socketServer)
        ;
    }

    public toJson() {
        return {
            uri: this.getUri(),
            host: this.host,
            port: this.port,
            client: this.client,
            socket: this.socketServer.toJson(),
        };
    }
}
