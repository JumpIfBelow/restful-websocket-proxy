import { Subject } from "rxjs";
import { Datagram } from "@enum/datagram.enum";
import { tcp } from "@function/tcp.function";
import { udp } from "@function/udp.function";
import { EqualsInterface } from "@interface/equals.interface";

export class Socket implements EqualsInterface, ToJsonInterface {
    protected socket$?: Subject<Buffer>;

    public constructor(
        public readonly host: string,
        public readonly port: number,
        public readonly datagram: Datagram,
    ) {
    }

    public bindSocket(): Subject<Buffer> {
        return this.socket$ = this.datagram === Datagram.TCP
            ? tcp(this.host, this.port)
            : udp(this.host, this.port, this.datagram)
        ;
    }

    public unbindSocket(): void {
        this.socket$?.complete();
        this.socket$ = undefined;
    }

    public equals(v: any): boolean {
        if (!(v instanceof Socket)) {
            return false;
        }

        return this === v
            || this.host === v.host
            && this.port === v.port
            && this.datagram === v.datagram
        ;
    }

    public toJson() {
        return {
            host: this.host,
            port: this.port,
            datagram: this.datagram,
        };
    }
}
