import { Router } from "express";
import { DeleteSocketProxyController } from "./delete-socket-proxy.controller";
import { GetSocketProxyController } from "./get-socket-proxy.controller";
import { PostSocketProxyController } from "./post-socket-proxy.controller";

export const SocketProxyController = Router().use(
    DeleteSocketProxyController,
    GetSocketProxyController,
    PostSocketProxyController,
);
