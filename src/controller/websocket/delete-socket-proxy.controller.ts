import { HttpError } from "@model/http-error.class";
import { SocketProxyFactoryService } from "@service/socket-proxy-factory.service";
import { SocketProxyManagerService } from "@service/socket-proxy-manager.service";
import { Router } from "express";

const router = Router();

router
    .route('/socket-proxies/:port')
    .delete((req, res, next) => {
        const port = parseInt(req.params.port);

        const socketProxy = SocketProxyManagerService
            .getSocketProxies()
            .find(socketProxy => socketProxy.port === port)
        ;

        if (!socketProxy) {
            res
                .status(404)
                .send(new HttpError(404, 'Not found.'))
            ;

            next();
            return;
        }

        SocketProxyFactoryService.destroySocketProxy(socketProxy);
        SocketProxyManagerService.remove(socketProxy);

        res
            .status(204)
            .send()
        ;
        next();
    })
;

export const DeleteSocketProxyController = router;
