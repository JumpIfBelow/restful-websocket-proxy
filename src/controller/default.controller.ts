import { Router } from 'express';
import { SocketProxyController } from './websocket/socket-proxy.controller';

export const DefaultController = Router().use(
    SocketProxyController,
);
