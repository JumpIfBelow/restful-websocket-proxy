export enum Datagram {
    TCP = 'tcp',
    UDP4 = 'udp4',
    UDP6 = 'udp6',
}
