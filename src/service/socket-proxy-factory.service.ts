import { Socket } from "@model/socket.class";
import { SocketProxy } from "@model/socket-proxy.class";
import { AvailablePorts, AvailablePortsService } from "./available-ports.service";

export class SocketProxyFactory {
    public constructor(
        protected readonly availablePortService: AvailablePorts,
    ) { }

    public createSocketProxy(host: string, client: string, socket: Socket): SocketProxy {
        const webSocketPort = this
            .availablePortService
            .hold()
        ;

        if (!webSocketPort) {
            throw new Error('There is no client port available to create new socket.');
        }

        const socketProxy = new SocketProxy(
            host,
            webSocketPort,
            client,
            socket,
        );

        return socketProxy;
    }

    public destroySocketProxy(socketProxy: SocketProxy): void {
        this
            .availablePortService
            .release(socketProxy.port)
        ;

        socketProxy
            .socketServer
            .unbindSocket()
        ;

        socketProxy
            .webSocketClients
            .forEach(webSocket => webSocket.close())
        ;
    }
}

export const SocketProxyFactoryService = new SocketProxyFactory(AvailablePortsService);
