import { Range } from "@type/range.type";
import config from '../../config.cjs';

export class AvailablePorts {
    protected ports: number[];
    protected usedPorts: number[];

    public constructor(range: Range) {
        this.ports = [];
        this.usedPorts = [];

        for (const port of range) {
            if (Array.isArray(port)) {
                for (let i = port[0]; i <= port[1]; i++) {
                    this.ports.push(i);
                }
            } else {
                this.ports.push(port);
            }
        }
    }

    /**
     * Finds the first port available and locks it off future usages.
     * @returns The port to use. False if none are available.
     */
    public hold(): number | false {
        const availablePorts = this
            .ports
            .filter(p => !this.usedPorts.includes(p))
        ;

        if (availablePorts.length === 0) {
            return false;
        }

        const port = availablePorts[0];
        this
            .usedPorts
            .push(port)
        ;

        return port;
    }

    /**
     * Declares a port as released, meaning that it can be used again. Therefore, it might
     * be returned by the {@see hold} method.
     * @param port The port to release
     * @returns True if the operation was successful.
     */
    public release(port: number): boolean {
        const portKey = this
            .usedPorts
            .indexOf(port)
        ;

        if (portKey === -1) {
            return false;
        }

        this
            .usedPorts
            .splice(portKey, 1)
        ;

        return true;
    }
}

export const AvailablePortsService = new AvailablePorts(<Range> config.websocket.ports);
