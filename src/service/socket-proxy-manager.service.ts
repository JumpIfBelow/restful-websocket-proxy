import { EventType } from "@enum/event-type.enum";
import { ItemEvent } from "@interface/item-event.interface";
import { SocketProxy } from "@model/socket-proxy.class";
import { Observable, Subject } from "rxjs";

export class SocketProxyManager {
    public readonly socketProxyEvent$: Observable<ItemEvent<SocketProxy>>;
    protected readonly socketProxyEventSubject$: Subject<ItemEvent<SocketProxy>>;

    public constructor(
        protected readonly socketProxies: SocketProxy[] = [],
    ) {
        this.socketProxyEventSubject$ = new Subject();
        this.socketProxyEvent$ = this.socketProxyEventSubject$.asObservable();
    }

    public getSocketProxies(): SocketProxy[] {
        return [...this.socketProxies];
    }

    public add(socketProxy: SocketProxy): boolean {
        if (this.socketProxies.find(w => w.equals(socketProxy))) {
            return false;
        }

        this.socketProxies.push(socketProxy);
        this.socketProxyEventSubject$.next({ event: EventType.CREATE, item: socketProxy });

        return true;
    }

    public remove(socketProxy: SocketProxy): boolean {
        const index = this
            .socketProxies
            .findIndex(w => w.equals(socketProxy))
        ;

        if (index === -1) {
            return false;
        }

        this
            .socketProxies
            .splice(index, 1)
        ;
        this.socketProxyEventSubject$.next({ event: EventType.DELETE, item: socketProxy });

        return true;
    }
}

export const SocketProxyManagerService = new SocketProxyManager();
